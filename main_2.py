import itertools


def get_dict_with_baskets_and_list_with_products(file_name):
    dict_with_baskets = {}

    set_with_products = set()

    with open(file_name, 'r') as file:
        for line in file:
            line = line.rstrip()
            value, key = line.split(";")

            if key not in dict_with_baskets.keys():
                dict_with_baskets[key] = []

            dict_with_baskets[key].append(value)
            set_with_products.add(value)

    return dict_with_baskets, list(set_with_products)


def pass_1(dict_with_baskets, list_with_products):
    product_count = {product: 0 for product in list_with_products}
    for value in dict_with_baskets.values():
        for product in value:
            product_count[product] += 1

    return product_count


def pass_2(dict_with_baskets, list_with_products):
    dicts_with_hashes = [{'count': 0, 'doubletons': []} for i in range(len(list_with_products))]

    k = len(list_with_products)

    map_table = {product: list_with_products.index(product) + 1 for product in list_with_products}

    for value in dict_with_baskets.values():
        list_with_doubletons = itertools.combinations(value, 2)

        for product_1, product_2 in list_with_doubletons:
            hash_key = sum((map_table[product_1], map_table[product_2])) % k
            dicts_with_hashes[hash_key]['count'] += 1
            dicts_with_hashes[hash_key]['doubletons'].append((product_1, product_2))

    return dicts_with_hashes


def main():
    support_level = 2
    dict_with_baskets, list_with_products = get_dict_with_baskets_and_list_with_products("transactions.csv")

    products_count = pass_1(dict_with_baskets, list_with_products)
    dict_with_hashes = pass_2(dict_with_baskets, list_with_products)

    result = []

    for dict_with_hash in dict_with_hashes:
        for product_1, product_2 in dict_with_hash['doubletons']:
            count = dict_with_hash['count']

            if products_count[product_1] >= support_level and products_count[product_2] >= support_level and count >= support_level:
                result.append((product_1, product_2))

    with open("result.txt", "w+") as file:
        for doubleton in result:
            file.write(f"{doubleton}\n")


if __name__ == '__main__':
    main()

